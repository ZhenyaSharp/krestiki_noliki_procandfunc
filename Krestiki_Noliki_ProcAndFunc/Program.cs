﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Krestiki_Noliki_ProcAndFunc
{
    class Program
    {
        enum Cell
        {
            Empty,
            Krestik,
            Nolik
        }

        enum Step
        {
            Player1,
            Player2,
            Draw
        }

        static void ClearField(Cell[,] field)
        {
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    field[i, j] = Cell.Empty;
                }
            }
        }
        static void PrintField(Cell[,] field)
        {
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for (int j = 0; j < field.GetLength(1); j++)
                {
                    switch (field[i, j])
                    {
                        case Cell.Empty:
                            Console.Write(".");
                            break;
                        case Cell.Krestik:
                            Console.Write("X");
                            break;
                        case Cell.Nolik:
                            Console.Write("O");
                            break;
                    }
                }
                Console.WriteLine();
            }
        }

        static void PrintGameField(Cell[,] field)
        {
            Console.WriteLine("KRESTIKI_NOLIKI");
            PrintField(field);
            Console.WriteLine();
            Console.ReadKey();
        }

        static void StepPlayer1(Cell[,] field)
        {
            bool inputResultNumI, inputResultNumJ;
            int numCellI, numCellJ;

            Console.WriteLine("Игрок №1 ваш ход: ");

            do
            {
                Console.Write("Введите номер строчки: ");
                inputResultNumI = int.TryParse(Console.ReadLine(), out numCellI);

                Console.Write("Введите номер столбца: ");
                inputResultNumJ = int.TryParse(Console.ReadLine(), out numCellJ);

            } while (inputResultNumI == false || inputResultNumJ == false ||
             numCellI - 1 < 0 || numCellI - 1 > field.GetLength(0) - 1 || numCellJ - 1 < 0 ||
             numCellJ - 1 > field.GetLength(1) - 1 || field[numCellI - 1, numCellJ - 1] == Cell.Krestik
             || field[numCellI - 1, numCellJ - 1] == Cell.Nolik);

            field[numCellI - 1, numCellJ - 1] = Cell.Krestik;
            IsWin(field);
        }

        static void StepPlayer2(Cell[,] field, int countStep)
        {
            bool inputResultNumI, inputResultNumJ;
            int numCellI, numCellJ;

            Console.WriteLine("Игрок №2 ваш ход: ");

            do
            {
                Console.Write("Введите номер строчки: ");
                inputResultNumI = int.TryParse(Console.ReadLine(), out numCellI);

                Console.Write("Введите номер столбца: ");
                inputResultNumJ = int.TryParse(Console.ReadLine(), out numCellJ);

            } while (inputResultNumI == false || inputResultNumJ == false ||
            numCellI - 1 < 0 || numCellI - 1 > field.GetLength(0) - 1 || numCellJ - 1 < 0 ||
            numCellJ - 1 > field.GetLength(1) - 1 || field[numCellI - 1, numCellJ - 1] == Cell.Krestik
            || field[numCellI - 1, numCellJ - 1] == Cell.Nolik);

            field[numCellI - 1, numCellJ - 1] = Cell.Nolik;
            IsWin(field);
        }

        static Step IsWin(Cell[,] field)
        {
            int countStep = 0;
            countStep++;

            if (field[0, 0] == Cell.Krestik && field[0, 1] == Cell.Krestik && field[0, 2] == Cell.Krestik || field[0, 0] == Cell.Krestik && field[1, 0] == Cell.Krestik && field[2, 0] == Cell.Krestik ||
                              field[0, 1] == Cell.Krestik && field[1, 1] == Cell.Krestik && field[2, 1] == Cell.Krestik || field[0, 2] == Cell.Krestik && field[1, 2] == Cell.Krestik && field[2, 2] == Cell.Krestik ||
                              field[1, 0] == Cell.Krestik && field[1, 1] == Cell.Krestik && field[1, 2] == Cell.Krestik || field[2, 0] == Cell.Krestik && field[2, 1] == Cell.Krestik && field[2, 2] == Cell.Krestik ||
                              field[0, 0] == Cell.Krestik && field[1, 1] == Cell.Krestik && field[2, 2] == Cell.Krestik || field[0, 2] == Cell.Krestik && field[1, 1] == Cell.Krestik && field[2, 0] == Cell.Krestik)
            {
                return Step.Player1;
            }

            if (field[0, 0] == Cell.Nolik && field[0, 1] == Cell.Nolik && field[0, 2] == Cell.Nolik || field[0, 0] == Cell.Nolik && field[1, 0] == Cell.Nolik && field[2, 0] == Cell.Nolik ||
                             field[0, 1] == Cell.Nolik && field[1, 1] == Cell.Nolik && field[2, 1] == Cell.Nolik || field[0, 2] == Cell.Nolik && field[1, 2] == Cell.Nolik && field[2, 2] == Cell.Nolik ||
                             field[1, 0] == Cell.Nolik && field[1, 1] == Cell.Nolik && field[1, 2] == Cell.Nolik || field[2, 0] == Cell.Nolik && field[2, 1] == Cell.Nolik && field[2, 2] == Cell.Nolik ||
                             field[0, 0] == Cell.Nolik && field[1, 1] == Cell.Nolik && field[2, 2] == Cell.Nolik || field[0, 2] == Cell.Nolik && field[1, 1] == Cell.Nolik && field[2, 0] == Cell.Nolik)
            {
                return Step.Player2;
            }

            if (countStep == 9)
            {
                return Step.Draw;
            }
            return 0;
        }

        static void PrintWinner(Step winner)
        {
            switch (winner)
            {
                case Step.Player1:
                    Console.WriteLine("PLAYER1 WIN!!!");
                    break;
                case Step.Player2:
                    Console.WriteLine("PLAYER2 WIN!!!");
                    break;
                case Step.Draw:
                    Console.WriteLine("DRAW!!!");
                    break;
            }
        }
        static void Main(string[] args)
        {
            int fieldSize = 3;
            Cell[,] field = new Cell[fieldSize, fieldSize];
            bool playGame = true;

            Step currentStep = Step.Player1;
            Step winner = Step.Player1;

            ClearField(field);

            while (playGame)
            {
                Console.Clear();

                PrintGameField(field);

                if (currentStep == Step.Player1)
                {
                    currentStep = StepPlayer1(field,);
                }
                else if (currentStep == Step.Player2)
                {
                    currentStep = StepPlayer2(field, );
                }
                if (IsWin(field,))
                {
                    playGame = false;
                    winner = currentStep;
                }
            }


        }
    }
}
